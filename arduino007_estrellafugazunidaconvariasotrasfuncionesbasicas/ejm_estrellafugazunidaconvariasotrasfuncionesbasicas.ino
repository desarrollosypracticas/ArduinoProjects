/*
Objetivo: Juntar la práctica de la estrella fugaz con la practica
15 y que corra.
*/

const int led1=13; //LED pata 13
const int led2=12; //LED pata 12
const int led3=11; //LED pata 11
const int led4=10; //LED pata 10
const int led5=9;  //LED pata 9

int pinArray[]={3,4,5,6,7,8,9,10,11,12,13,12,11,10,9,8,7,6,5,4,3};

int controlLed=3; //LED de control

int waitNextLed=100; //Tiempo antes de encender el siguiente LED

//Número de LED-s que permanecen encendidos antes de empezar a apagarlos
//para formar la cola.

int tailLength=3;

//Número de LED-s conectados (que es también el tamaño del array)

int lineSize=22;

void setup(){

 pinMode(led1,OUTPUT); //Digital, como salida
 pinMode(led2,OUTPUT); //Digital, como salida
 pinMode(led3,OUTPUT); //Digital, como salida
 pinMode(led4,OUTPUT); //Digital, como salida
 pinMode(led5,OUTPUT); //Digital, como salida
 
 int i;
 
 pinMode(controlLed,OUTPUT);
 
 for(i=0;i<lineSize;i++){
 
   pinMode(pinArray[i],OUTPUT);
 
 
 }//End for


}//End setup


void loop(){

digitalWrite(led1,HIGH); //ON

delay(1000);//espera

digitalWrite(led1,LOW); //OFF

delay(250); //ESPERA

digitalWrite(led2,HIGH); //ON

delay(1000); //espera

digitalWrite(led2,LOW); //OFF

delay(250); //ESPERA

digitalWrite(led3,HIGH); //ON

delay(1000); //espera

digitalWrite(led3,LOW); //OFF

delay(250); //ESPERA

digitalWrite(led4,HIGH); //ON

delay(1000); //espera

digitalWrite(led4,LOW); //OFF

delay(250); //ESPERA

digitalWrite(led5,HIGH); //ON

delay(1000); //espera

digitalWrite(led5,LOW); //OFF

delay(250); //ESPERA

digitalWrite(led5,HIGH); //ON

delay(1000); //espera

digitalWrite(led5,LOW); //OFF

delay(250); //ESPERA

digitalWrite(led4,HIGH); //ON

delay(1000); //espera

digitalWrite(led4,LOW); //OFF

delay(250); //ESPERA

digitalWrite(led3,HIGH); //ON

delay(1000); //espera

digitalWrite(led3,LOW); //OFF

delay(250); //ESPERA

digitalWrite(led2,HIGH); //ON

delay(1000); //espera

digitalWrite(led2,LOW); //OFF

delay(250); //ESPERA

digitalWrite(led1,HIGH); //ON

delay(1000); //espera

digitalWrite(led1,LOW); //OFF

delay(250); //ESPERA


digitalWrite(led5,HIGH); //ON

delay(1000); //espera

digitalWrite(led5,LOW); //OFF

delay(250); //ESPERA

digitalWrite(led3,HIGH); //ON

delay(1000); //espera

digitalWrite(led3,LOW);// OFF

delay(250); //ESPERA

digitalWrite(led1,HIGH); //ON

delay(1000); //ON

digitalWrite(led1,LOW); //OFF

delay(250); //ESPERA

digitalWrite(led1,HIGH); //ON

delay(1000); //espera

digitalWrite(led1,LOW); //OFF

delay(250); //ESPERA

digitalWrite(led3,HIGH); //ON

delay(1000); //espera

digitalWrite(led3,LOW); //OFF

delay(250); //ESPERA

digitalWrite(led5,HIGH); //ON

delay(1000); //espera

digitalWrite(led5,LOW); //OFF

delay(250); //ESPERA


int i;

//Se establece la longitud de lacola en un contador

int tailCounter=tailLength;

//Se enciende el LED de control para indicar el inicio del loop

digitalWrite(controlLed,HIGH);

for(i=0;i<lineSize;i++){

 digitalWrite(pinArray[i],HIGH); //Se encienden consecutivamente los LED
 
 //Esta variable de tiempo controla la velocidad a la que se mueve la estrella
 
 delay(waitNextLed);
 
 if(tailCounter==0){
 
 //Se apagan los LED-s en funcion de la longitud de la cola.
 
 digitalWrite(pinArray[i-tailLength],LOW);
   
 }//End if
 else{
 
 if(tailCounter>0){
 
 tailCounter--;
 
 }//End second if
 
 }//End else
  
}//End for


for(i=(lineSize-tailLength);i<lineSize;i++){

 digitalWrite(pinArray[i],LOW); //Se apagan los LED
 
 //Esta variable de tiempo controla la velocidad a la que se mueve la estrella
 
 delay(waitNextLed);

}//End for

}//End loop
