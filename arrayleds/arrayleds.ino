const int buttonPin1=1,buttonPin2=2;
const int led[]= {3,4,5,6,7,8,9};
int buttonState = 0,time=100; // variable for reading the pushbutton status
void setup() //declara la variable en output
{
for(int i=3;i<10;i++)
{
pinMode(led[i], OUTPUT);
}
// initialize the pushbutton pin as an input:
pinMode(buttonPin1, INPUT);
pinMode(buttonPin2, INPUT);
}

void loop() //ciclos
{
// read the state of the pushbutton value:
buttonState = digitalRead(buttonPin1);
do{
buttonState = digitalRead(buttonPin1);
if (buttonState == HIGH)
{
// ENCENDIDO IZQUIERDA-DERECHA
for(int i=0;i<7;i++)
derecha();
}
else
{
for(int i=0;i<7;i++)
{
digitalWrite(led[i], LOW);
}
}
} while(buttonState==HIGH);

buttonState = digitalRead(buttonPin2);
do{
buttonState = digitalRead(buttonPin2);
if (buttonState == HIGH)
{
//ENCENDIDO DERECHA-IZQUIERDA 
for(int i=0;i<7;i++)
izquierda();
}
else
{
for(int i=0;i<7;i++)
{
digitalWrite(led[i], LOW);
}
}
}while(buttonState==HIGH);
}
void izquierda()
{
for(int i=7;i>=0;i--)
{
digitalWrite(led[i], HIGH);
delay(time);
digitalWrite(led[i], LOW);
delay(time);
}
}

void derecha()
{
for(int i=0;i<=7;i++)
{
digitalWrite(led[i], HIGH);
delay(time);
digitalWrite(led[i], LOW);
delay(time);
}
}

