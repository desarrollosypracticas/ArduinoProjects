/*Objetivo: Realizar un corrimiento de Led´s de al menos 5 que inicie de izquierda a derecha.*/

const int led1=13; //LED pata 13
const int led2=12; //LED pata 12
const int led3=11; //LED pata 11
const int led4=10; //LED pata 10
const int led5=9;  //LED pata 9

void setup(){

  pinMode(led1,OUTPUT); //Digital,como salida
  
  pinMode(led2,OUTPUT); //Digital,como salida
 
  pinMode(led3,OUTPUT); //Digital,como salida
 
  pinMode(led4,OUTPUT); //Digital,como salida
 
  pinMode(led5,OUTPUT); //Digital,como salida
 
  
  
}// end setup


void loop(){

  digitalWrite(led1,HIGH); //ON
  
  delay(2000); //espera
  
  digitalWrite(led1,LOW); //OFF
  
  delay(250); //Espera
  
  digitalWrite(led2,HIGH); //ON
  
  delay(2000); //espera
  
  digitalWrite(led2,LOW); //OFF
  
  delay(250); //Espera
  
  digitalWrite(led3,HIGH); //ON
  
  delay(2000); //Espera
  
  digitalWrite(led3,LOW);
  
  delay(250); //espera
  
  digitalWrite(led4,HIGH); //ON
  
  delay(2000); //espera
  
  digitalWrite(led4,LOW); //OFF
  
  delay(250); //espera
  
  digitalWrite(led5,HIGH); //ON
  
  delay(2000); //espera
  
  digitalWrite(led5,LOW);
  
  delay(250); //espera
  
}//End loop
