//Parpadeo de LED, Hola Mundo de microcontroladores
void setup(){
  pinMode(13,OUTPUT); //declaramos el pin 13 como salida
    
}

void loop(){
  digitalWrite(13,HIGH); //encendemos el LED
  delay(1000); //esperamos un segundo
  digitalWrite(13,LOW); //apagamos el LED
  delay(1000); //esperamos un segundo 
}
