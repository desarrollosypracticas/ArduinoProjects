/*Objetivo: Encender y apagar un led de 1000ms y un tiempo de apagado de 1000ms.

Blink
*/
//Pin 13 has an LED connected on most Arduino boards.
//give it a name:
int led=13;

//the setup routine runs once when you press reset:
void setup(){
//initialize the digital pin as an output.
pinMode(led,OUTPUT);

}//End setup

//the loop routine runs over and over again forever:
void loop(){
digitalWrite(led,HIGH);//turn the led on (HIGH is the voltage level)
delay(1000);//wait for a second
digitalWrite(led,LOW);//turn the LED off by making the voltage LOW.
delay(1000);//wait for a second

}//End loop

/*Utilizando digitalWrite se indica que la acción se lleve a cabo ya sea high para encender o low
para apagar, y el delay para indicar el tiempo de retardo.*/
