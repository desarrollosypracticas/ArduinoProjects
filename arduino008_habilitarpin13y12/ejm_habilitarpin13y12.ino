/*Objetivo: Habilitar el pin 13 y el 12 y hacer la secuencia de encendido a 2000ms en el pin 13 y
10ms en el pin 12, después a la inversa.*/

int led1=13;
int led2=12;

void setup(){

pinMode(led1,OUTPUT);

pinMode(led2,OUTPUT);

}//End setup

void loop(){

digitalWrite(led1,HIGH);//1

delay(2000);

digitalWrite(led1,LOW);

delay(2000);

digitalWrite(led2,HIGH);//1

delay(100);

digitalWrite(led2,LOW);

delay(100);

digitalWrite(led2,HIGH);//1

delay(2000);

digitalWrite(led2,LOW);

delay(2000);

digitalWrite(led1,HIGH);//1

delay(100);

digitalWrite(led1,LOW);

delay(100);

}//End loop
