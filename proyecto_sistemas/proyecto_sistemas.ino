#include "DHT.h"
#define DHTPIN 2
#define DHTTYPE DHT11 
DHT dht(DHTPIN, DHTTYPE);
 
           const int pinSensor = A0;     // pin del sensor de temperatura, va a la pata central del sensor
	   const int LedVent    = 13;      // pin para el led

	   const int umbral    = 400;     // umbral de la temperatura, ya en grados centigrados

	   int valorSensor = 0;          // variable para guardar el valor leido del sensor

	   float temperatura = 0;        // variable para guardar la temperatura

           const int LEDLuz=12;
           const int LDRPin=A3;
           const int threshold = 1000;
           const int rejaPin=11;


void setup() {
  
        pinMode(LEDLuz, OUTPUT);
  	pinMode(LDRPin, INPUT);
	       // declaramos el pin del Led de salida
	       pinMode(LedVent, OUTPUT);
                pinMode(rejaPin,OUTPUT);
	       //inicializamos la comunicacion serial
	       Serial.begin(9600); 
                dht.begin();
	   }//End setup



void loop() {
  digitalWrite(rejaPin, HIGH);
  
  /////////////////Inicializa funcionamiento de Temperatura///////////////////
  
	       // leemos el valor del sensor
	       valorSensor = analogRead(pinSensor);
//Serial.println(valorSensor);
	       // este valor lo convertimos a milivolts
	       float milivolts = (valorSensor / 1023.0) * 5000;
	       // y lo convertimos a grados centigrados 
	       //  1 grado centigrado = 10 milivolts
	       temperatura = milivolts/10;

	       Serial.print("La temperatura es de: ");
	       Serial.print(temperatura);
	       Serial.println(" grados centigrados");

	       // comparamos el valor de la temperatura con el umbral
	       if (temperatura > umbral){
	          digitalWrite(LedVent, HIGH);
	       } else {
	          digitalWrite(LedVent, LOW);
	       }
//////////////Inicializa funcionamiento de Humedad//////////////////////////
          int h = dht.readHumidity();// Lee la humedad
Serial.print("Humedad Relativa: ");                 
Serial.print(h);//Escribe la humedad
Serial.println(" %");                     


/////////////Inicializa funcionamiento de Iluminación/////////////////////
int input = analogRead(LDRPin);
Serial.print("Luminosidad: "); 
Serial.print(input);//Escribe la humedad
Serial.println("> que 1000"); 
	if (input > threshold) {
		digitalWrite(LEDLuz, LOW);
	}
	else {
		digitalWrite(LEDLuz, HIGH);
	}
delay(1000);


}//End loop

