 const int Btn=3;
  const int L=13;
  int EBtn=0;

void setup() {
  // put your setup code here, to run once:
  
  pinMode(L,OUTPUT);//LED
  pinMode(Btn,INPUT);//Boton
}//Corre una vez cuando el programa inicia

void loop() {
  // put your main code here, to run repeatedly:
  EBtn=digitalRead(Btn);
  if(EBtn==HIGH){
  digitalWrite(L,HIGH);//Manda 5 volts al pin #4, para encenderlo
  //delay(1000);//Tiempo de espera
  }else{
      digitalWrite(L,LOW);
    }
  //digitalWrite(10,LOW);//pin #4, para apagarlo
  //delay(1000);//Tiempo de espera
}//Se ejecuta repetidamente
